﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomersGrpc1;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
//using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomersServer : CustomersGrpc1.customers.customersBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersServer(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        public override async Task<GetCustomersResponse> GetCustomers(GetCustomersRequest request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id.ToString("N"),
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName
            }).ToList();

            return new GetCustomersResponse
            {
                Items = { response },
            };

        }

        public override async Task<GetCustomerResponse> GetCustomer(GetCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new GetCustomerResponse
            {
                Id = customer.Id.ToString("N"),
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = {
                    customer.Preferences.Select(x => new PreferenceResponse
                    {
                        Id = x.PreferenceId.ToString("N"),
                        Name = x.Preference.Name
                    }).ToList()
                },
            };

            return response;
        }

        public override async Task<CreateCustomerResponse> CreateCustomer(CreateCustomerRequest request, ServerCallContext context)
        {
            //Получаем предпочтения из бд и сохраняем большой объект
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.Customer.PreferenceIds.Select(p => Guid.Parse(p)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request.Customer, preferences);

            await _customerRepository.AddAsync(customer);

            return new CreateCustomerResponse { Id = customer.Id.ToString("N") };
        }

        public override async Task<EditCustomersResponse> EditCustomers(EditCustomersRequest request, ServerCallContext context)
        {

            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new EditCustomersResponse { Error = 404 };

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(
                request.Customer.PreferenceIds.Select(x => Guid.Parse(x)).ToList());

            CustomerMapper.MapFromModel(request.Customer, preferences, customer);

            await _customerRepository.UpdateAsync(customer);
            return new EditCustomersResponse();
        }

        public override async Task<DeleteCustomerResponse> DeleteCustomer(DeleteCustomerRequest request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));
            if (customer == null)
                return new DeleteCustomerResponse
                {
                    Error = 404,
                };

            await _customerRepository.DeleteAsync(customer);

            return new DeleteCustomerResponse();
        }
    }
}